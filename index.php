<?php

/* 
 * Example index - few simple scenarios to be printed 
 * Only used to invoke logic and show valdiator outputs
 * */

require_once("./bootstrap.php");

use \App\Promotion;

// vouchers to test --> thise IDs are used also in dummy data source passed as "db" to App\Promotion\Promotion
$testedPromotions = [
    "W8R3TTNR", "ofd", "paused", "Not found", "wdas", "oou", "sep"
];

foreach ($testedPromotions as $promoID) {
    echo PHP_EOL;
    $promo = Promotion\PromotionFactory::create();
    // Load promo - if not exists or does not belongs to client print message "not found.."
    if (!$promo->load($promoID) || !$promo->validateBelongings("abc")) {
        echo "Promotion id [" . $promoID . "] not found" . PHP_EOL;
        continue;
    }
    
    $validationResult = $promo->validate();
    // If voucher can be used
    if ($validationResult === true) {
        echo "Promotion [" . $promo->getPromotionTitle() . "] is VALID" . PHP_EOL;
        continue;
    }
    // If voucher cannot be used
    echo "Promotion [" . $promo->getPromotionTitle() . "] s not valid, see reasons below" . PHP_EOL;
    foreach ($validationResult as $err)
        echo $err . PHP_EOL;
}


