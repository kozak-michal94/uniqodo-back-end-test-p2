<?php

namespace App\Promotion;

class PromotionFactory {

    /**
     * Create promotion and search for promotion through factory - pass array as DB..
     * @return Promotion
     */
    public static function create(): Promotion {
        $yesterday = $date = new \DateTime();
        $yesterday->add(\DateInterval::createFromDateString('yesterday'));

        $tomorrow = new \DateTime();
        $tomorrow->add(\DateInterval::createFromDateString('tomorrow'));

        $dummyData = [[
            "title" => "Working voucher",
            "status" => PromotionStatusEnum::Active, 
            "promotionID" => "W8R3TTNR",
            "start" => $yesterday,
            "end" => $tomorrow,
            "clientID" => "abc",
            "allowance" => 4
        ], [
            "title" => "Out of date",
            "status" => PromotionStatusEnum::Active, 
            "promotionID" => "ofd",
            "start" => $tomorrow,
            "end" => $tomorrow,
            "clientID" => "abc",
            "allowance" => 4
        ], [
            "title" => "Paused",
            "status" => PromotionStatusEnum::Paused, 
            "promotionID" => "paused",
            "start" => $yesterday,
            "end" => $tomorrow,
            "clientID" => "abc",
            "allowance" => 4
        ], [
            "title" => "Wrong date and status",
            "status" => PromotionStatusEnum::Paused, 
            "promotionID" => "wdas",
            "start" => $yesterday,
            "end" => $yesterday,
            "clientID" => "abc",
            "allowance" => 4
        ], [
            "title" => "Out of usage",
            "status" => PromotionStatusEnum::Active,
            "promotionID" => "oou",
            "start" => $yesterday,
            "end" => $tomorrow,
            "clientID" => "abc",
            "allowance" => 1
        ], [
            "title" => "Someone else promo",
            "status" => PromotionStatusEnum::Active,
            "promotionID" => "sep",
            "start" => $yesterday,
            "end" => $tomorrow,
            "clientID" => "sep",
            "allowance" => 1
        ]];
        // Something like usage records
        $dummyRecords = [[
            "promoID" => "W8R3TTNR",
            "date" => $yesterday
        ], [
            "promoID" => "oou",
            "date" => $yesterday
        ]];

        return new Promotion([
            "promos" => $dummyData,
            "promoRecords" => $dummyRecords
        ]);
    }

}

