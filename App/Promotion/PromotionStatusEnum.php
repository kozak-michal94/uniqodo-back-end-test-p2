<?php

namespace App\Promotion;

class PromotionStatusEnum {

    const Active = true;
    const Paused = false;

}

