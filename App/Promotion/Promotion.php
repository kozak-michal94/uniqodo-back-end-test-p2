<?php

namespace App\Promotion;

class Promotion {

    private $promotionData;
    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    function load($promotionID) {
        // Just choose correct record from db which is currently array
        $res = reset(array_filter($this->db["promos"], function($i) use($promotionID) {
            return $i["promotionID"] == $promotionID;
        }));
        if (!$res)
            return false;
        $this->promotionData = $res;
        return $this;
    }

    function getUsageRecords() {
        // Fake selection -> take it from array
        return array_filter($this->db["promoRecords"], function($rec) {
            return $rec["promoID"] === $this->promotionData["promotionID"];
        });
    }

    function validate() {
        $errs = [];
        // Status check
        if ($this->promotionData["status"] == PromotionStatusEnum::Paused)
            array_push($errs, "Promotion is currently paused");
        // Time range check
        if (!$this->validateDate(new \DateTime("now")))
            array_push($errs, "Voucher is out of available time range");
        if (!$this->validateUsageAllowance())
            array_push($errs, "Voucher has exceeded usage allowance");
        
        return count($errs) ? $errs : true;
    }

    function getPromotionTitle(): string {
        return $this->promotionData["title"];
    }

    function validateBelongings($clientID) {
        return $clientID === $this->promotionData["clientID"];
    }

    function validateUsageAllowance() {
        return (count($this->getUsageRecords()) < $this->promotionData["allowance"]);
    }

    function validateDate($inputDate) {
        // Time range check
        if (
            $inputDate->getTimestamp() < $this->promotionData["start"]->getTimestamp() ||
            $inputDate->getTimestamp() > $this->promotionData["end"]->getTimestamp()
        )
            return false;
        return true;
    }

}
